# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|N|

## Website Detail Description

# Basic Components Description : 
1. Jucify mechanisms : [關卡 : 總共有五個關卡，在主頁面可以看到level到level5的按鈕，每一個關卡間都可以按previous level和next level轉換關卡(第一關第                          五關除外)，也有home按鍵回主頁面。
                        技能 : 除了第一關之外每一關都有技能，普通攻擊也需要0.7秒冷卻時間，目的是講求技術流，timing必須抓準]
2. Animations : [角色動畫 : 有加角色動畫，會變換深淺，頻率1秒]
3. Particle Systems : [??]
4. Sound effects : [射擊、敵人射擊、使用技能、撞到敵人、擊中敵人、被擊中都會有音效]
5. Leaderboard : [無]

# Bonus Functions Description : 
1. [初始頁面] : [有一個按鈕how to play，點進入頁面後有how to play，可以看到動畫頁面，裡面有簡單介紹遊戲玩法跟遊戲規則]
2. [遊戲畫面] : [左上角玩家血量，遭一顆子彈擊中會扣一滴血，總共有十滴血，右上角有記分板(雖然好像沒什麼用，因為我沒有加leaderboard功能)，左下角有小幫手功                 能]
3. [Level 1] : [1.只有正常攻擊，發出藍色子彈，擊中敵人產生爆炸
                2.敵人左右晃動，會瞄準玩家發射子彈，玩家被擊中產生爆炸
                3.擊殺所有敵人後勝利]
4. [Level 2] : [1.正常攻擊，發出藍色子彈，擊中敵人產生爆炸，技能Z(冷卻8秒)發射特殊子彈，可以產生範圍傷害，但如果擊中衝撞型敵人則無效，效果如同一般爆炸
                2.兩種敵人，一種原地左右晃動，會瞄準玩家發射子彈，玩家被擊中產生爆炸，另一種敵人向玩家方向衝撞，被撞到後玩家損血並減速
                3.擊殺所有原地晃動敵人後勝利]
5. [Level 3] : [1.正常攻擊，發出藍色子彈，擊中敵人產生爆炸，技能Z三連發(冷卻8秒)
                2.兩種敵人，一種是Boss，原地左右晃動，另一種敵人向玩家方向衝撞，同時會瞄準玩家發射子彈，玩家被擊中產生爆炸，被撞到後玩家損血並減速
                  (Boss血量顯示在下方，Level 3 Boss血量55滴)
                3.擊殺Boss後勝利]
6. [Level 4] : [1.正常攻擊，發出藍色子彈，擊中敵人產生爆炸，技能Z三連發(冷卻8秒)，技能X放射狀發射6顆子彈(冷卻12秒)
                2.四種敵人，一隻是Boss和三隻guard，原地左右晃動，另兩種敵人向玩家方向衝撞，一種會瞄準玩家發射子彈，速度較慢，玩家被擊中產生爆炸，一種速度快，衝撞敵人，兩種撞到後玩家損血並減速
                  (Boss和guard血量顯示在下方，Level 4 Boss血量60滴，三隻guard血量各15滴)
                3.擊殺Boss後勝利]
7. [Level 5] : [1.正常攻擊，發出藍色子彈，擊中敵人產生爆炸，技能Z三連發(冷卻8秒)，技能X放射狀發射6顆子彈(冷卻12秒)，技能C啟動護盾(冷卻60秒)，一次啟動能                 抵銷10次子彈攻擊，但是無法抵銷衝撞攻擊)
                2.五種敵人，一隻是Boss和一隻特殊guard二隻普通guard，原地左右晃動，另兩種敵人向玩家方向衝撞，一種會瞄準玩家發射子彈，速度較慢，玩家被擊中產生爆炸，一種速度快，衝撞敵人，兩種撞到後玩家損血並減速
                (Boss和guard血量顯示在下方，Level 5 Boss血量75滴，兔書guard血量30滴，兩隻普通guard血量各20滴)
                3.特殊guard遭玩家擊中會反擊兩顆直線子彈，Boss遭玩家擊中會放射狀反擊五顆子彈，
                4.擊殺Boss後勝利]
