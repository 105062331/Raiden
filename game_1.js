
var game = new Phaser.Game(800, 700, Phaser.AUTO, 'phaser-example', { preload: preload, create: create, update: update, render: render });

function preload() {

    game.load.image('bullet', 'assets/bullet.png');
    ///game.load.image('bullet_1', 'assets/bullet_1.png');
    game.load.image('enemyBullet', 'assets/enemy-bullet.png');
    game.load.spritesheet('invader', 'assets/invader.png', 32, 32);
    game.load.image('ship', 'assets/player.png');
    game.load.image('heart', 'assets/heart.png');
    game.load.spritesheet('kaboom', 'assets/explode.png', 128, 128);
    game.load.spritesheet('aircraft', 'assets/aircraft.png', 64, 64);
    game.load.image('starfield', 'assets/dawn.png');
    game.load.image('background', 'assets/background2.png');
    ///audio
    game.load.audio('shot1', 'audio/shot1.wav');
    game.load.audio('e_death', 'audio/explosion_e.mp3');
    game.load.audio('sword', 'audio/sword.mp3');
    //game.load.audio('bg', 'audio/Game of Thrones.mp3');
}

var keys;///
var shot_1;///
var e_death;
var bg;
var sword;
var player;
var aliens;
var bullets;
var bullets_1;
var cursors;
var fireButton;
var fireButton_1;
var explosions;
var explosions_1;
var starfield;
var lives;
var scoreText;
var liveText;
var enemyBullets;
var stateText;
var score = 0;
var firingTimer = 0;
var bulletTime = 0;
var cur_live = 5;
var livingEnemies = [];
var scoreString = '';
var liveString = '';
var pauseButton;
var pauseText;

function create() {

    bulletTime = 0;
    game.physics.startSystem(Phaser.Physics.ARCADE);
    //  The scrolling starfield background
    starfield = game.add.tileSprite(0, 0, 800, 700, 'starfield');
    //audio
    shot_1 = game.add.audio('shot1');///
    e_death = game.add.audio('e_death');///
    sword = game.add.audio('sword');///
    bg = game.add.audio('bg');///
    ///bg.play();
    ///game.sound.setDecodedCallback(shot_1, start, this);
    //game.sound.setDecodedCallback(p_death, collision_audio, this);///
    //  Our bullet group
    bullets = game.add.group();
    bullets.enableBody = true;
    bullets.physicsBodyType = Phaser.Physics.ARCADE;
    bullets.createMultiple(30, 'bullet');
    bullets.setAll('anchor.x', 0.5);
    bullets.setAll('anchor.y', 1);
    bullets.setAll('outOfBoundsKill', true);
    bullets.setAll('checkWorldBounds', true);
    // The enemy's bullets
    enemyBullets = game.add.group();
    enemyBullets.enableBody = true;
    enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
    enemyBullets.createMultiple(30, 'enemyBullet');
    enemyBullets.setAll('anchor.x', 0.5);
    enemyBullets.setAll('anchor.y', 1);
    enemyBullets.setAll('outOfBoundsKill', true);
    enemyBullets.setAll('checkWorldBounds', true);

    //  The hero!
    /*player = game.add.sprite(400, 500, 'ship');
    player.anchor.setTo(0.5, 0.5);
    game.physics.enable(player, Phaser.Physics.ARCADE);*/
    player = game.add.sprite(400, 500, 'aircraft');
    player.anchor.setTo(0.5, 0.5);
    game.physics.enable(player, Phaser.Physics.ARCADE);
    player.animations.add('fly');
    player.play('fly', 1000, true);
    //  The baddies!
    aliens = game.add.group();
    aliens.enableBody = true;
    aliens.physicsBodyType = Phaser.Physics.ARCADE;

    createAliens();

    //  The score
    scoreString = 'Score : ';
    scoreText = game.add.text(game.world.width - 150, 10, scoreString + score, { font: '20px Arial', fill: '#FFF' });

    //  Lives
    liveString = 'Lives : ';
    liveText = game.add.text(10, 10, liveString, { font: '20px Arial', fill: '#FFF' });
    lives = game.add.group();

    pauseText = game.add.text(10, 500, 'P : pause\nSPACE : normal attack', { font: '13px Arial', fill: '#FFF' });
    ///game.add.text(game.world.width - 150, 10, liveString, { font: '34px Arial', fill: '#fff' });

    //  Text
    stateText = game.add.text(game.world.centerX,game.world.centerY,' ', { font: '84px Arial', fill: '#FFF' });
    stateText.anchor.setTo(0.5, 0.5);
    stateText.visible = false;

    for(var k = 0; k < 2; k++){

        for (var i = 0; i < 5; i++) {
            var ship = lives.create(15 + (30 * i), 60 + (30 * k), 'heart');
            ship.anchor.setTo(0.5, 0.5);
            ///ship.angle = 90;
            ship.alpha = 0.8;
        }
    }

    //  An explosion pool
    explosions = game.add.group();
    explosions.createMultiple(30, 'kaboom');
    explosions.forEach(setupInvader, this);
    //  And some controls to play the game with
    cursors = game.input.keyboard.createCursorKeys();
    fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    ///pauseButton = game.input.keyboard.addKey(Phaser.Keyboard.P);
    window.onkeydown = function(event) {
        
        if (event.keyCode == 80){
            game.paused = !game.paused;
        }
        if(game.paused === false){

            stateText.visible = false;
        }
        else{

            stateText.text=" PAUSE \n Press P To Resume";
            stateText.visible = true;
        }
    }
}
/*function start(){///

    ///shot_1.onStop.add(soundStopped, this);
    keys = game.input.keyboard.addKeys({shot_1 : Phaser.Keyboard.SPACEBAR});
    keys.shot_1.onDown.add(playFx, this);
}

function playFx(key){///

    switch(key.keyCode){

        case Phaser.Keyboard.SPACEBAR : 
            shot_1.play();
            break;
    }
}*/

function createAliens () {

    for (var y = 0; y < 2; y++)
    {
        for (var x = 0; x < 8; x++)
        {
            var alien = aliens.create(x * 80, y * 50, 'invader');
            alien.anchor.setTo(0.5, 0.5);
            ///alien.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
            ///alien.play('fly');
            alien.body.moves = false;
        }
    }

    aliens.x = 100;
    aliens.y = 50;

    //  All this does is basically start the invaders moving. Notice we're moving the Group they belong to, rather than the invaders directly.
    var tween = game.add.tween(aliens).to( { x: 200 }, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);

    //  When the tween loops it calls descend
    tween.onLoop.add(descend, this);
}

function setupInvader (invader) {

    invader.anchor.x = 0.5;
    invader.anchor.y = 0.5;
    invader.animations.add('kaboom');

}

function descend() {

    aliens.y += 10;

}

function update() {

    //  Scroll the background
    bg.play();
    starfield.tilePosition.y += 2;

    if (player.alive)
    {
        //  Reset the player, then check for movement keys
        player.body.velocity.setTo(0, 0);

        if (cursors.left.isDown)
        {
            player.body.velocity.x = -250;
        }
        else if (cursors.right.isDown)
        {
            player.body.velocity.x = 250;
        }
        else if(cursors.up.isDown){

            player.body.velocity.y = -250;
        }
        else if(cursors.down.isDown){

            player.body.velocity.y = 250;
        }
        if(cursors.left.isDown && cursors.up.isDown){

            player.body.velocity.x = -250;
            player.body.velocity.y = -250;
        }
        if(cursors.left.isDown && cursors.down.isDown){

            player.body.velocity.x = -250;
            player.body.velocity.y = 250;
        }
        if(cursors.right.isDown && cursors.down.isDown){

            player.body.velocity.x = 250;
            player.body.velocity.y = 250;
        }
        if(cursors.right.isDown && cursors.up.isDown){

            player.body.velocity.x = 250;
            player.body.velocity.y = -250;
        }
        //  Firing?
        if (fireButton.isDown)
        {
            fireBullet();
        }
        /*else if(pauseButton.isDown){

            pauseAct();
        }*/
        if (game.time.now > firingTimer)
        {
            enemyFires();
        }

        //  Run collision
        game.physics.arcade.overlap(bullets, aliens, collisionHandler, null, this);
        ///game.physics.arcade.overlap(bullets_1, aliens, collisionHandler_1, null, this);
        game.physics.arcade.overlap(enemyBullets, player, enemyHitsPlayer, null, this);
    }

}

function render() {

    // for (var i = 0; i < aliens.length; i++)
    // {
    //     game.debug.body(aliens.children[i]);
    // }

}
/*function collision_audio(){

    collisionHandler(bullets, aliens);
}*/
function collisionHandler (bullet, alien) {

    //  When a bullet hits an alien we kill them both
    bullet.kill();
    alien.kill();
    //audio
    e_death.play();
    //  Increase the score
    score += 20;
    scoreText.text = scoreString + score;

    //  And create an explosion :)
    var explosion = explosions.getFirstExists(false);
    explosion.reset(alien.body.x, alien.body.y);
    explosion.play('kaboom', 30, false, true);

    if (aliens.countLiving() == 0)
    {
        score += 1000;
        scoreText.text = scoreString + score;

        enemyBullets.callAll('kill',this);
        stateText.text = " You Win !! \n Click to restart";
        stateText.visible = true;

        //the "click to restart" handler
        game.input.onTap.addOnce(restart,this);
    }

}

function enemyHitsPlayer (player,bullet) {
    
    bullet.kill();
    //audio
    e_death.play();
    cur_live -= 1;
    ///liveText.text = liveString + cur_live;
    liveText.text = liveString;
    live = lives.getFirstAlive();

    if (live)
    {
        live.kill();
    }

    //  And create an explosion :)
    var explosion = explosions.getFirstExists(false);
    explosion.reset(player.body.x + 24, player.body.y + 42);
    explosion.play('kaboom', 30, false, true);

    // When the player dies
    if (lives.countLiving() < 1)
    {
        ///cur_live = 5;
        player.kill();
        enemyBullets.callAll('kill');

        stateText.text=" GAME OVER \n Click to restart";
        stateText.visible = true;

        //the "click to restart" handler
        game.input.onTap.addOnce(restart,this);
    }

}

function enemyFires () {

    //audio
    //sword.play();
    //  Grab the first bullet we can from the pool
    enemyBullet = enemyBullets.getFirstExists(false);

    livingEnemies.length=0;

    aliens.forEachAlive(function(alien){

        // put every living enemy in an array
        livingEnemies.push(alien);
    });


    if (enemyBullet && livingEnemies.length > 0)
    {
        
        sword.play();
        var random=game.rnd.integerInRange(0,livingEnemies.length-1);

        // randomly select one of them
        var shooter=livingEnemies[random];
        // And fire the bullet from this enemy
        enemyBullet.reset(shooter.body.x, shooter.body.y);

        game.physics.arcade.moveToObject(enemyBullet,player,300);
        firingTimer = game.time.now + 3000;
    }

}

function fireBullet () {

    //  To avoid them being allowed to fire too fast we set a time limit
    if (game.time.now > bulletTime)
    {
        shot_1.play();
        //  Grab the first bullet we can from the pool
        bullet = bullets.getFirstExists(false);

        if (bullet)
        {
            //  And fire it
            bullet.reset(player.x, player.y + 8);
            bullet.body.velocity.y = -400;
            bulletTime = game.time.now + 700;
        }
    }

}
/*function pauseAct(){

    if(game.paused){

        game.paused = false;
        stateText.visible = false;        
    }
    else{

        game.paused = true;
        stateText.text=" PAUSE \n Press P To Resume";
        stateText.visible = true;
    }
}*/

function resetBullet (bullet) {

    //  Called if the bullet goes out of the screen
    bullet.kill();

}

function restart () {

    //  A new level starts
    /*cur_live = 5;
    //resets the life count
    lives.callAll('revive');
    //  And brings the aliens back from the dead :)
    aliens.removeAll();
    createAliens();

    //revives the player
    player.revive();
    //hides the text
    stateText.visible = false;
    score = 0;
    scoreText.text = scoreString + score;*/
    create();
}
